use warnings;
use lib qw(.);

use Bugzilla;

my $cgi = Bugzilla->cgi;
my $package = $cgi->param('package');
$package =~ s,/,\|,; 

my $additional_info;
if ($cgi->param('os')) {
    $additional_info .= "OS: " . $cgi->param('os') if $cgi->param('os');
    $additional_info .= "\n" if $cgi->param('compiler');
}
if ($cgi->param('compiler')) {
    $additional_info .= "Compiler: " . $cgi->param('compiler');
}

my $product;
my $component;
if ($cgi->param('package') =~ m,^(.*)\|(.*)$,) {
    $product = $1;
    $component = $2;
} elsif ($cgi->param('package') =~ m,^(.*)/(.*)$,) {
    $product = $1;
    $component = $2;
} else {
    $product = $cgi->param('package');
}

my $newReportPage = $cgi->url(-base => 1);
$newReportPage .= "/enter_bug.cgi?format=guided";
$newReportPage .= "&product=";
$newReportPage .= $cgi->escape($product);
$newReportPage .= "&component=" . $cgi->escape($component) if ($component);
$newReportPage .= "&additional_info=" . $cgi->escape($additional_info) if ($additional_info);
$newReportPage .= "&version=" . $cgi->param('appVersion') if ($cgi->param('appVersion'));
print STDERR "new url for bug reporting is $newReportPage\n";
print STDERR "Redirecting now\n";
print $cgi->redirect($newReportPage);
return 0;
