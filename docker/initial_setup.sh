#!/bin/bash

if [ ! -d ../lib ]; then
  mkdir ../lib;
fi

if [ ! -d ../data ]; then
  mkdir ../data;
fi


docker-compose up -d --build && docker-compose exec apache bash -c 'perl checksetup.pl docker/checksetup_answers.txt && chown -R '$(id -u)' .'